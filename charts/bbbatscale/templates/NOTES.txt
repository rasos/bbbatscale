{{- if .Values.ingress.enabled }}
Visit one of the following URLs to use your BBB@Scale instance:
{{- range .Values.bbbatscale.domains }}
http{{ if or $.Values.ingress.tls (hasKey $.Values.ingress.annotations "route.openshift.io/termination") }}s{{ end }}://{{ . }}
{{- end }}
{{- else if eq "NodePort" .Values.nginx.serviceType }}
The BBB@Scale instance can be visited via the address of your node and the port {{ .Values.nginx.port }}.
This might be something like http://ip.of.your.node:{{ .Values.nginx.port }}.
{{- else if eq "LoadBalancer" .Values.nginx.serviceType }}
The BBB@Scale instance can be visited via the address of your LoadBalancer Ingress and the port {{ .Values.nginx.port }}.
This might be something like http://ip.of.your.ingress:{{ .Values.nginx.port }}.
The external ip of your instance can be retrieved with the following command (it may take a while for the ip to get available).
  kubectl get services nginx-service
{{- else }}
To use your BBB@Scale instance, execute the following command and visit http://127.0.0.1:8080.
  kubectl --namespace {{ .Release.Namespace | quote }} port-forward service/nginx-service 8080:{{ .Values.nginx.port }}
{{- end }}
