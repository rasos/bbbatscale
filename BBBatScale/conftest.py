from typing import Iterator

import pytest
from channels.db import database_sync_to_async
from django.contrib.sites.models import Site


@pytest.fixture(scope="function")
def testserver_tenant(db) -> Site:
    return Site.objects.create(name="testserver", domain="testserver")


@pytest.fixture(scope="function")
def testserver2_tenant(db) -> Site:
    return Site.objects.create(domain="testserver2", name="testserver2")


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def async_example_tenant(db) -> Iterator[Site]:
    tenant = await database_sync_to_async(lambda: Site.objects.create(name="example.org", domain="example.org"))()
    yield tenant
    await database_sync_to_async(tenant.delete)()
