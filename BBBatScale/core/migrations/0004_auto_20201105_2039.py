# Generated by Django 3.1.2 on 2020-11-05 20:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0003_auto_20201029_2203"),
    ]

    operations = [
        migrations.AlterField(
            model_name="room",
            name="is_public",
            field=models.BooleanField(default=False),
        ),
    ]
