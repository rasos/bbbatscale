# encoding: utf-8
import logging
from datetime import timedelta

from core.constants import ROOM_STATE_CREATING
from core.models import Room
from core.services import reset_room
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils.timezone import now
from tendo.singleton import SingleInstance, SingleInstanceException

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Reset all rooms which are stucked in creating state for 60sec and more."

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start reset for stucked rooms")

        try:
            SingleInstance()
            logger.debug("Start reset_room for rooms that were in creating state for more than 60 sec")
            for room in Room.objects.filter(last_running__lte=now() - timedelta(seconds=60), state=ROOM_STATE_CREATING):
                reset_room(room.meeting_id, room.name, room.config)
                logger.error("Room ({}) was in state 'creating' for more than 60 seconds. Has been reset!".format(room))
            logger.debug("End reset_room")

        except SingleInstanceException as e:
            logger.error("An error occurred during the reset. Details:" + str(e))

        logger.info("End reset_stucked_rooms command ")
