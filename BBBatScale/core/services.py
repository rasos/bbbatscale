import json
import logging
import re

from core.event_collectors.base import EventCollectorContext
from django.conf import settings
from django.db.models import OuterRef, Subquery
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext
from xmltodict import unparse

from .constants import (
    GUEST_POLICY_ALLOW,
    GUEST_POLICY_APPROVAL,
    MODERATION_MODE_ALL,
    MODERATION_MODE_MODERATORS,
    MODERATION_MODE_STARTER,
    ROOM_STATE_INACTIVE,
    SERVER_STATE_ERROR,
)
from .models import (
    ExternalRoom,
    HomeRoom,
    Meeting,
    PersonalRoom,
    Room,
    RoomEvent,
    Server,
    User,
    get_default_room_config,
)
from .utils import BigBlueButton

logger = logging.getLogger(__name__)


def moderator_message(request, name, access_code, only_prompt_guests_for_access_code, guest_policy, max_participants):
    logger.debug(
        f"Moderator message will be created with following parameters: name: {name}, access_code: {access_code},"
        f" only_prompt_guests_for_access_code: {only_prompt_guests_for_access_code}, guest_policy: {guest_policy},"
        f" maxParticipants: {max_participants}"
    )
    join_url = request.build_absolute_uri(reverse("join_redirect", args=[name]))
    message = gettext("Meeting link: {}").format(join_url)
    if access_code:
        if only_prompt_guests_for_access_code:
            ac_message = gettext("This room is protected by an access code only for guests: {}").format(access_code)
        else:
            ac_message = gettext("This room is protected by an access code: {}").format(access_code)
    else:
        ac_message = None
    gp_message = gettext("The room has a guest lobby enabled") if guest_policy == GUEST_POLICY_APPROVAL else None
    max_participants_message = (
        gettext("The room is limited to {} participants").format(max_participants) if max_participants else None
    )
    logger.debug("Moderator message created.")
    return "<br/>".join(filter(None, [message, ac_message, gp_message, max_participants_message]))


def create_web_hook_bbb(request, bbb):
    logger.debug("Creating web hook for {} on {}.".format(bbb.url, request.get_host()))
    web_hook_params = {"callbackURL": request.build_absolute_uri(reverse("callback_bbb"))}
    return bbb.create_web_hook(web_hook_params)


def create_meeting_and_bbb_parameter(request, room, create_parameter):
    try:
        if room.is_externalroom():
            logger.debug("Creating parameter for External BBB meeting.")
            meeting = Meeting.objects.create(room_name=room.name, meeting_id=room.meeting_id, creator="External")
            logger.debug(f"External meeting created for {meeting.room_name}.")
            create_parameter.update(
                {
                    "meta_creator": "External",
                    "meta_roomsmeetingid": meeting.pk,
                    "meta_muteonstart": False
                    if "mute_on_start" not in create_parameter.keys()
                    else create_parameter["mute_on_start"],
                    "meta_moderationmode": MODERATION_MODE_MODERATORS,
                    "meta_guestpolicy": GUEST_POLICY_ALLOW,
                    "meta_allowguestentry": False,
                    "meta_accesscode": None,
                    "meta_onlypromptguestsforaccesscode": False,
                    "meta_disablecam": False,
                    "meta_disablemic": False,
                    "meta_disablenote": False,
                    "meta_disableprivatechat": False,
                    "meta_disablepublicchat": False,
                    "meta_allowrecording": True if create_parameter["record"] == "true" else False,
                }
            )
            logger.debug(f"External meeting parameter created: {create_parameter}.")
            return create_parameter
        logger.debug("Creating parameter for BBB Meeting.")
        meeting = Meeting.objects.create(
            room_name=room.name, meeting_id=room.meeting_id, creator=create_parameter["creator"]
        )
        logger.debug(f"Meeting created for {meeting.room_name} by {meeting.creator}.")
        _create_parameter = {
            "name": create_parameter["name"],
            "meetingID": create_parameter["meeting_id"],
            "attendeePW": create_parameter["attendee_pw"],
            "moderatorPW": create_parameter["moderator_pw"],
            "logoutURL": create_parameter["logoutUrl"]
            if create_parameter["logoutUrl"]
            else request.build_absolute_uri(reverse("home")),
            "muteOnStart": "true" if create_parameter["mute_on_start"] else "false",
            "moderatorOnlyMessage": moderator_message(
                request,
                create_parameter["name"],
                create_parameter["access_code"],
                create_parameter["only_prompt_guests_for_access_code"],
                create_parameter["guest_policy"],
                create_parameter["maxParticipants"],
            ),
            "lockSettingsDisableCam": "true" if create_parameter["disable_cam"] else "false",
            "lockSettingsDisableMic": "true" if create_parameter["disable_mic"] else "false",
            "lockSettingsDisableNote": "true" if create_parameter["disable_note"] else "false",
            "lockSettingsDisablePublicChat": "true" if create_parameter["disable_public_chat"] else "false",
            "lockSettingsDisablePrivateChat": "true" if create_parameter["disable_private_chat"] else "false",
            "guestPolicy": create_parameter["guest_policy"],
            "record": "true" if create_parameter["allow_recording"] else "false",
            "allowStartStopRecording": "true" if create_parameter["allow_recording"] else "false",
            "meta_creator": create_parameter["creator"],
            "meta_roomsmeetingid": meeting.pk,
            "meta_muteonstart": create_parameter["mute_on_start"],
            "meta_moderationmode": create_parameter["moderation_mode"],
            "meta_guestpolicy": create_parameter["guest_policy"],
            "meta_allowguestentry": create_parameter["allow_guest_entry"],
            "meta_accesscode": create_parameter["access_code"],
            "meta_onlypromptguestsforaccesscode": create_parameter["only_prompt_guests_for_access_code"],
            "meta_disablecam": create_parameter["disable_cam"],
            "meta_disablemic": create_parameter["disable_mic"],
            "meta_disablenote": create_parameter["disable_note"],
            "meta_disableprivatechat": create_parameter["disable_private_chat"],
            "meta_disablepublicchat": create_parameter["disable_public_chat"],
            "meta_allowrecording": create_parameter["allow_recording"],
            "meta_logouturl": create_parameter["logoutUrl"],
            "meta_maxParticipants": create_parameter["maxParticipants"],
            "meta_streamingUrl": create_parameter["streamingUrl"],
        }
        if create_parameter["welcome_message"]:
            _create_parameter["welcome"] = create_parameter["welcome_message"]
        if create_parameter["dialNumber"]:
            _create_parameter["dialNumber"] = create_parameter["dialNumber"]
        if create_parameter["maxParticipants"]:
            _create_parameter["maxParticipants"] = create_parameter["maxParticipants"]
        logger.debug(f"Meeting parameter created: {_create_parameter}.")
        return _create_parameter
    except KeyError as e:
        logger.critical(f"Create meeting aborted. Key error {str(e)}")
        return None


def meeting_create(request, create_parameter, room=None, body=None):
    logger.debug("Start creating meeting.")
    # create Webhook & Return Server
    _room = room or Room.objects.get(meeting_id=create_parameter["meeting_id"])
    server = _room.scheduling_strategy.get_server_for_room()
    logger.debug(f"Selected server for {_room.name} is {server.dns}.")
    bbb = BigBlueButton(server.dns, server.shared_secret)
    web_hook_response = create_web_hook_bbb(request, bbb)
    if BigBlueButton.validate_create_web_hook(web_hook_response):
        logger.debug(f"Webhook created on {server.dns}.")
        try:
            response = bbb.create(create_meeting_and_bbb_parameter(request, _room, create_parameter), body)
            logger.debug("Meeting created on {} with meeting id: {} .".format(server.dns, _room.meeting_id))
            logger.debug("End creating meeting.")
            if _room.is_externalroom():
                return response
            return BigBlueButton.validate_create(response)
        except (ConnectionError, TimeoutError) as e:
            logger.error(f"Create meeting on server {server.dns} took longer than expected. Error message: {str(e)}")
            return True
    else:
        Server.objects.filter(pk=server.pk).update(state=SERVER_STATE_ERROR)
        logger.critical(f"Could not create webhook at {server.dns}. Server state is set to ERROR.")
        return False if not _room.is_externalroom() else web_hook_response


def create_join_meeting_url(meeting_id, user, password):
    room = Room.objects.get(meeting_id=meeting_id)
    logger.debug(f"Join URL for {room.server} creation.")
    join_parameter = {"meetingID": room.meeting_id, "password": password, "fullName": str(user), "redirect": "true"}
    if isinstance(user, User):
        join_parameter["userdata-bbb_auto_join_audio"] = json.dumps(user.bbb_auto_join_audio)
        join_parameter["userdata-bbb_listen_only_mode"] = json.dumps(user.bbb_listen_only_mode)
        join_parameter["userdata-bbb_skip_check_audio"] = json.dumps(user.bbb_skip_check_audio)
        join_parameter["userdata-bbb_skip_check_audio_on_first_join"] = json.dumps(
            user.bbb_skip_check_audio_on_first_join
        )
        join_parameter["userdata-bbb_auto_share_webcam"] = json.dumps(user.bbb_auto_share_webcam)
        join_parameter["userdata-bbb_record_video"] = json.dumps(user.bbb_record_video)
        join_parameter["userdata-bbb_skip_video_preview"] = json.dumps(user.bbb_skip_video_preview)
        join_parameter["userdata-bbb_skip_video_preview_on_first_join"] = json.dumps(
            user.bbb_skip_video_preview_on_first_join
        )
        join_parameter["userdata-bbb_mirror_own_webcam"] = json.dumps(user.bbb_mirror_own_webcam)
        join_parameter["userdata-bbb_force_restore_presentation_on_new_events"] = json.dumps(
            user.bbb_force_restore_presentation_on_new_events
        )
        join_parameter["userdata-bbb_auto_swap_layout"] = json.dumps(user.bbb_auto_swap_layout)
        join_parameter["userdata-bbb_show_participants_on_login"] = json.dumps(user.bbb_show_participants_on_login)
        join_parameter["userdata-bbb_show_public_chat_on_login"] = json.dumps(user.bbb_show_public_chat_on_login)

    logger.debug(f"Join parameter: {join_parameter}.")
    bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
    return bbb.join(join_parameter)


def meeting_end(meeting_id, pw):
    meeting = Room.objects.get(meeting_id=meeting_id)
    logger.debug(f"Ending meeting for {meeting.name} on server {meeting.server}.")
    return BigBlueButton(meeting.server.dns, meeting.server.shared_secret).end(meeting_id, pw)


def apply_room_config(meeting_id, config):
    logger.debug(f"Applying room configuration for room with meeting id: {meeting_id} .")
    Room.objects.filter(meeting_id=meeting_id).update(
        mute_on_start=config.mute_on_start,
        moderation_mode=config.moderation_mode,
        everyone_can_start=config.everyone_can_start,
        authenticated_user_can_start=config.authenticated_user_can_start,
        guest_policy=config.guest_policy,
        allow_guest_entry=config.allow_guest_entry,
        access_code=config.access_code,
        only_prompt_guests_for_access_code=config.only_prompt_guests_for_access_code,
        allow_recording=config.allow_recording,
        disable_cam=config.disable_cam,
        disable_mic=config.disable_mic,
        disable_note=config.disable_note,
        url=config.mute_on_start,
        dialNumber=config.dialNumber,
        logoutUrl=config.logoutUrl,
        welcome_message=config.welcome_message,
        maxParticipants=config.maxParticipants,
        streamingUrl=config.streamingUrl,
    )


def reset_room(meeting_id, room_name, config):
    logger.debug("Start resetting room {}".format(room_name))
    if config:
        logger.debug(f"Config for resetting room was supplied: {config}")
        Room.objects.filter(meeting_id=meeting_id).update(
            server=None,
            state=ROOM_STATE_INACTIVE,
            participant_count=0,
            videostream_count=0,
            last_running=None,
            mute_on_start=config.mute_on_start,
            moderation_mode=config.moderation_mode,
            guest_policy=config.guest_policy,
            allow_guest_entry=config.allow_guest_entry,
            access_code=config.access_code,
            only_prompt_guests_for_access_code=config.only_prompt_guests_for_access_code,
            disable_cam=config.disable_cam,
            disable_mic=config.disable_mic,
            disable_note=config.disable_note,
            disable_private_chat=config.disable_private_chat,
            disable_public_chat=config.disable_public_chat,
            allow_recording=config.allow_recording,
            url=config.url,
            logoutUrl=config.logoutUrl,
            welcome_message=config.welcome_message,
            dialNumber=config.dialNumber,
            maxParticipants=config.maxParticipants,
            streamingUrl=config.streamingUrl,
        )
    else:
        logger.debug(
            "No config was supplied. Resetting essential values: server, state, "
            "last running and video&participant count"
        )
        Room.objects.filter(meeting_id=meeting_id).update(
            server=None,
            state=ROOM_STATE_INACTIVE,
            participant_count=0,
            videostream_count=0,
            last_running=None,
        )
    logger.debug("End resetting room {}".format(room_name))


def collect_room_occupancy(room_pk):
    room = Room.objects.get(pk=room_pk)
    logger.debug(f"Starting collection of room occupancy for {room.name}.")
    context = EventCollectorContext(room.event_collection_strategy)
    context.collect_events(room_pk, room.event_collection_parameters)
    logger.debug(f"Ended collection of room occupancy for {room.name}.")


def room_click(room_pk):
    from django.db.models import F

    Room.objects.filter(pk=room_pk).update(click_counter=F("click_counter") + 1)
    logger.debug(f"Updated click counter for {room_pk}.")


def get_rooms_with_current_next_event(tenant):
    logger.debug("Adding current and next event to Room query.")
    current = RoomEvent.objects.filter(
        room=OuterRef("pk"), start__lte=timezone.now(), end__gte=timezone.now()
    ).order_by("end")
    next = RoomEvent.objects.filter(room=OuterRef("pk"), start__gt=timezone.now(), end__gt=timezone.now()).order_by(
        "start"
    )
    return (
        Room.objects.filter(tenants__in=[tenant], is_breakout=False, config__isnull=False, is_public=True)
        .exclude(pk__in=Subquery(ExternalRoom.objects.values_list("pk", flat=True)))
        .exclude(pk__in=Subquery(PersonalRoom.objects.values_list("pk", flat=True)))
        .exclude(pk__in=Subquery(HomeRoom.objects.values_list("pk", flat=True)))
        .annotate(room_occupancy_current=Subquery(current.values("name")[:1]))
        .annotate(room_occupancy_next=Subquery(next.values("name")[:1]))
        .prefetch_related("config")
    )


def get_join_password(user, _room: Room, creator_name):
    creator = Meeting.objects.filter(room_name=_room.name).first().creator
    if (
        user.is_superuser
        or (user.is_staff and user.tenant in _room.tenants.all())
        or (_room.moderation_mode == MODERATION_MODE_STARTER and creator == creator_name)
        or (_room.is_personalroom() and (_room.personalroom.owner == user or _room.personalroom.has_co_owner(user)))
    ):
        password = _room.moderator_pw
    elif _room.guest_policy == GUEST_POLICY_APPROVAL and not creator == creator_name:
        password = _room.attendee_pw
    elif _room.moderation_mode == MODERATION_MODE_ALL or (
        _room.moderation_mode == MODERATION_MODE_MODERATORS
        and user.groups.filter(name=settings.MODERATORS_GROUP).exists()
        and user.tenant in _room.tenants.all()
    ):
        password = _room.moderator_pw
    else:
        password = _room.attendee_pw
    logger.debug(f"Password for joining meeting {password} for user {user if user.is_authenticated else creator_name}")
    return password


def external_get_or_create_room_with_params(parameter, scheduling_strategy, tenant=None):
    if ExternalRoom.objects.filter(meeting_id=parameter["meetingID"]).exists():
        return ExternalRoom.objects.get(meeting_id=parameter["meetingID"])
    else:
        unique_name = remove_double_spaces(parameter["name"])
        while ExternalRoom.objects.filter(name=unique_name).exists():
            unique_name = "{}-{}".format(parameter["name"], timezone.now().time().isoformat()[-3:])
        parameter["name"] = unique_name
        logger.debug(f"Unique name for external room is: {unique_name} for {parameter['meetingID']}")
        logger.debug(f"Creating external room with {parameter}.")
        room, created = ExternalRoom.objects.get_or_create(
            name=parameter["name"],
            meeting_id=parameter["meetingID"],
            attendee_pw=parameter["attendeePW"],
            moderator_pw=parameter["moderatorPW"],
            scheduling_strategy=scheduling_strategy,
            config=get_default_room_config(),
        )
        if tenant:
            room.tenants.add(tenant)
    return room


def parse_recordings_from_multiple_sources(servers, params):
    logger.debug("Start collecting recordings from multiple servers.")
    recordings = []
    server: Server
    for server in servers:
        logger.debug(f"Collecting recordings from {server}")
        server_request = BigBlueButton.validate_get_recordings(
            BigBlueButton(server.dns, server.shared_secret).get_recordings(params)
        )
        if server_request["response"]["returncode"] == "SUCCESS" and "messageKey" not in server_request["response"]:
            if isinstance(server_request["response"]["recordings"]["recording"], list):
                logger.debug(f"Found multiple recordings on server {server}")
                for recording in server_request["response"]["recordings"]["recording"]:
                    if recording not in recordings:
                        logger.debug(f"Recording added to response array {recording}")
                        recordings.append(recording)
            else:
                if server_request["response"]["recordings"]["recording"] not in recordings:
                    logger.debug(f"Found one recordings on server {server}")
                    recordings.append(server_request["response"]["recordings"]["recording"])
    return (
        {"response": {"returncode": "SUCCESS", "recordings": {"recording": recordings}}}
        if len(recordings) >= 1
        else None
    )


def publish_recordings(servers, params):
    logger.debug("Start publishing recording.")
    for server in servers:
        logger.debug(f"Checking {server} for recording")
        _params = {"recordID": params["recordID"]}
        server_request = BigBlueButton.validate_get_recordings(
            BigBlueButton(server.dns, server.shared_secret).get_recordings(_params)
        )
        if server_request and not server_request["response"].get("messageKey"):
            logger.debug(f"Found recording {params['recordID']} on maschine {server}")
            return BigBlueButton(server.dns, server.shared_secret).publish_recordings(params)
    return None


def update_recordings(servers, params):
    logger.debug("Start updating recording.")
    for server in servers:
        logger.debug(f"Checking {server} for recording")
        _params = {"recordID": params["recordID"]}
        server_request = BigBlueButton.validate_get_recordings(
            BigBlueButton(server.dns, server.shared_secret).get_recordings(_params)
        )
        if server_request and not server_request["response"].get("messageKey"):
            logger.debug(f"Found recording {params['recordID']} on maschine {server}")
            return BigBlueButton(server.dns, server.shared_secret).update_recordings(params)
    return None


def delete_recordings(servers, params):
    logger.debug("Start delete recording.")
    for server in servers:
        logger.debug(f"Checking {server} for recording")
        _params = {"recordID": params["recordID"]}
        server_request = BigBlueButton.validate_get_recordings(
            BigBlueButton(server.dns, server.shared_secret).get_recordings(_params)
        )
        if server_request and not server_request["response"].get("messageKey"):
            logger.debug(f"Found recording {params['recordID']} on maschine {server}")
            return BigBlueButton(server.dns, server.shared_secret).delete_recordings(params)
    return None


def parse_dict_to_xml(response_dict):
    logger.debug("Parsing dict to XML.")
    return unparse(response_dict)


def join_or_create_meeting_permissions(user, room):
    logger.debug(f"Returning permission to create meeting for {user} and {room}")
    if room.everyone_can_start:
        return True
    if user.is_authenticated:
        if user.is_superuser:
            return True
        if room.is_homeroom():
            return room.homeroom.owner == user
        if room.is_personalroom():
            return room.personalroom.owner == user or room.personalroom.has_co_owner(user)
        if user.tenant in room.tenants.all():
            if room.authenticated_user_can_start:
                return True
            if user.groups.filter(name=settings.MODERATORS_GROUP).exists():
                return True
    return False


def remove_double_spaces(string):
    return re.sub(r"\s{2,}", " ", string)
