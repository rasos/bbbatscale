/**
 * @param {string} fetchUrl
 * @param {string} stateKey
 * @param {HTMLTableSectionElement} tableBody
 * @param {HTMLElement} noResultsLabel
 * @param {HTMLInputElement} search
 * @param {HTMLElement} paginationPrevious
 * @param {HTMLInputElement} paginationPage
 * @param {HTMLElement} paginationCount
 * @param {HTMLElement} paginationNext
 * @param {{label: HTMLElement, icon: HTMLElement}} [nameOrdering]
 * @param {{label: HTMLElement, icon: HTMLElement}} [ownerOrdering]
 * @param {{label: HTMLElement, icon: HTMLElement}} [stateOrdering]
 * @param {{label: HTMLElement, icon: HTMLElement}} [participantCountOrdering]
 * @param {boolean} [displayName=false]
 * @param {boolean} [displayOwner=false]
 * @param {boolean} [displayOccupancy=false]
 * @returns {void}
 */
function installUpdateRooms(
    fetchUrl,
    stateKey,
    {
        tableBody,
        noResultsLabel,
        search,
        paginationPrevious,
        paginationPage,
        paginationCount,
        paginationNext,
        nameOrdering,
        ownerOrdering,
        stateOrdering,
        participantCountOrdering,
        displayName = false,
        displayOwner = false,
        displayOccupancy = false
    }
) {
    const validOrderBy = [
        nameOrdering ? "name" : null,
        ownerOrdering ? "owner" : null,
        stateOrdering ? "state" : null,
        participantCountOrdering ? "participant_count" : null
    ].filter(Boolean);

    let state;
    try {
        state = JSON.parse(localStorage.getItem(stateKey) || "{}");
    } catch {
        state = {};
    }
    let {
        filter: roomFilter = "",
        page: roomPage = 1,
        orderBy: roomOrderBy = validOrderBy[0],
        descending: roomDescending = false
    } = state;
    search.value = roomFilter;

    /**
     * @param {string} [filter=roomFilter]
     * @param {number} [page=roomPage]
     * @param {"name" | "owner" | "state"} [orderBy=roomOrderBy]
     * @param {boolean} [descending=roomDescending]
     */
    function updateState({ filter = roomFilter, page = roomPage, orderBy = roomOrderBy, descending = roomDescending }) {
        roomFilter = filter;
        roomPage = page;
        roomOrderBy = orderBy;
        roomDescending = descending;
        localStorage.setItem(stateKey, JSON.stringify({ filter, page, orderBy, descending }));
    }

    /**
     * @typedef {Object} Room
     * @property {string} url
     * @property {string} name
     * @property {?string} owner
     * @property {"ACTIVE" | "INACTIVE" | "CREATING"} state
     * @property {string} configuration
     * @property {?string} current_occupancy
     * @property {?string} next_occupancy
     * @property {number} participants
     * @property {?string} comment
     */

    /**
     * @param {Room} data
     * @returns {HTMLTableRowElement}
     */
    function renderRoom(data) {
        const row = document.createElement("tr");

        const columnActions = document.createElement("td");
        row.append(columnActions);

        const columnActionsSpan = document.createElement("span");
        columnActionsSpan.style.whiteSpace = "nowrap";
        columnActions.append(columnActionsSpan);

        const columnActionsSpanCopy = document.createElement("em");
        columnActionsSpanCopy.classList.add("far", "fa-sm", "fa-copy");
        columnActionsSpanCopy.style.cursor = "pointer";
        columnActionsSpanCopy.addEventListener("click", () => copyRoomLinkToClipboard(data.url));
        columnActionsSpan.append(columnActionsSpanCopy);

        const columnActionsSpanMail = document.createElement("em");
        columnActionsSpanMail.classList.add("ml-1", "far", "fa-sm", "fa-envelope");
        columnActionsSpanMail.style.cursor = "pointer";
        columnActionsSpanMail.addEventListener("click", () => inviteToRoomViaEmail(data.name, data.url));
        columnActionsSpan.append(columnActionsSpanMail);

        if (displayName) {
            const columnName = document.createElement("td");
            row.append(columnName);

            const columnNameAnchor = document.createElement("a");
            columnNameAnchor.href = data.url;
            columnNameAnchor.innerText = data.name;
            columnName.append(columnNameAnchor);
        }

        if (displayOwner) {
            const columnOwner = document.createElement("td");
            row.append(columnOwner);

            if (displayName) {
                columnOwner.innerText = data.owner;
            } else {
                const columnOwnerAnchor = document.createElement("a");
                columnOwnerAnchor.href = data.url;
                columnOwnerAnchor.innerText = data.owner;
                columnOwner.append(columnOwnerAnchor);
            }
        }

        const columnState = document.createElement("td");
        row.append(columnState);

        const columnStateSpan = document.createElement("span");
        if (data.state === "ACTIVE") {
            columnStateSpan.classList.add("badge", "badge-success");
            columnStateSpan.innerText = gettext("active");
        } else if (data.state === "INACTIVE") {
            columnStateSpan.classList.add("badge", "badge-secondary");
            columnStateSpan.innerText = gettext("inactive");
        } else {
            columnStateSpan.classList.add("badge", "badge-warning");
            columnStateSpan.innerText = gettext("creating");
        }
        columnState.append(columnStateSpan);

        const columnConfiguration = document.createElement("td");
        columnConfiguration.innerText = data.configuration;
        row.append(columnConfiguration);

        if (displayOccupancy) {
            const columnCurrentOccupancy = document.createElement("td");
            columnCurrentOccupancy.classList.add("d-none", "d-lg-table-cell");
            columnCurrentOccupancy.innerText = data.current_occupancy || "";
            row.append(columnCurrentOccupancy);

            const columnNextOccupancy = document.createElement("td");
            columnNextOccupancy.classList.add("d-none", "d-lg-table-cell");
            columnNextOccupancy.innerText = data.next_occupancy || "";
            row.append(columnNextOccupancy);
        }

        const columnParticipants = document.createElement("td");
        columnParticipants.classList.add("d-none", "d-lg-table-cell");
        columnParticipants.innerText = data.participants.toFixed(0);
        row.append(columnParticipants);

        const columnComment = document.createElement("td");
        columnComment.classList.add("d-none", "d-lg-table-cell");
        columnComment.innerText = data.comment || "";
        row.append(columnComment);

        return row;
    }

    /**
     * @param {number} pageNumber
     * @param {number} pageCount
     */
    function updatePagination(pageNumber, pageCount) {
        paginationPrevious.disabled = pageNumber <= 1;
        paginationCount.innerText = "/ " + pageCount.toFixed(0);
        paginationPage.value = pageNumber.toFixed(0);
        paginationPage.max = pageCount.toFixed(0);
        paginationNext.disabled = pageNumber >= pageCount;
    }

    /**
     * @param {string} [filter=roomFilter]
     * @param {number} [page=roomPage]
     * @param {"name" | "owner" | "state" | "participant_count"} [orderBy=roomOrderBy]
     * @param {boolean} [descending=roomDescending]
     */
    function updateRooms({
        filter = roomFilter,
        page = roomPage,
        orderBy = roomOrderBy,
        descending = roomDescending
    } = {}) {
        if (!orderBy || !validOrderBy.includes(orderBy)) {
            orderBy = validOrderBy[0];
        }

        nameOrdering?.icon.classList.remove("fa-sort", "fa-sort-up", "fa-sort-down");
        ownerOrdering?.icon.classList.remove("fa-sort", "fa-sort-up", "fa-sort-down");
        stateOrdering?.icon.classList.remove("fa-sort", "fa-sort-up", "fa-sort-down");
        participantCountOrdering?.icon.classList.remove("fa-sort", "fa-sort-up", "fa-sort-down");
        if (orderBy === "name") {
            nameOrdering?.icon.classList.add(descending ? "fa-sort-down" : "fa-sort-up");
            ownerOrdering?.icon.classList.add("fa-sort");
            stateOrdering?.icon.classList.add("fa-sort");
            participantCountOrdering?.icon.classList.add("fa-sort");
        } else if (orderBy === "owner") {
            nameOrdering?.icon.classList.add("fa-sort");
            ownerOrdering?.icon.classList.add(descending ? "fa-sort-down" : "fa-sort-up");
            stateOrdering?.icon.classList.add("fa-sort");
            participantCountOrdering?.icon.classList.add("fa-sort");
        } else if (orderBy === "state") {
            nameOrdering?.icon.classList.add("fa-sort");
            ownerOrdering?.icon.classList.add("fa-sort");
            stateOrdering?.icon.classList.add(descending ? "fa-sort-down" : "fa-sort-up");
            participantCountOrdering?.icon.classList.add("fa-sort");
        } else if (orderBy === "participant_count") {
            nameOrdering?.icon.classList.add("fa-sort");
            ownerOrdering?.icon.classList.add("fa-sort");
            stateOrdering?.icon.classList.add("fa-sort");
            participantCountOrdering?.icon.classList.add(descending ? "fa-sort-down" : "fa-sort-up");
        }
        updateState({ filter, page, orderBy, descending });

        const url = new URL(fetchUrl, window.location);
        url.searchParams.set("filter", filter);
        url.searchParams.set("page", page.toFixed(0));
        url.searchParams.set("orderBy", orderBy);
        url.searchParams.set("descending", descending ? "true" : "false");
        fetch(url.toString())
            .then((response) => response.json())
            .then(
                /**
                 * @param {{page: number, page_count: number, rooms: Array<Room>}} data
                 */
                (data) => {
                    updateState({ page: data.page });

                    noResultsLabel.hidden = data.page_count !== 1 || data.rooms.length !== 0;

                    tableBody.innerText = "";
                    tableBody.append(...data.rooms.map((room) => renderRoom(room)));

                    updatePagination(data.page, data.page_count);
                }
            );
    }

    search.addEventListener("keyup", () => updateRooms({ filter: search.value }));
    paginationPrevious.addEventListener("click", () => updateRooms({ page: roomPage - 1 }));
    paginationPage.addEventListener("change", () =>
        updateRooms({ page: Math.floor(Number(paginationPage.value)) || 1 })
    );
    paginationNext.addEventListener("click", () => updateRooms({ page: roomPage + 1 }));
    nameOrdering?.label.addEventListener("click", () =>
        updateRooms({
            orderBy: "name",
            descending: nameOrdering.icon.classList.contains("fa-sort-up")
        })
    );
    ownerOrdering?.label.addEventListener("click", () =>
        updateRooms({
            orderBy: "owner",
            descending: ownerOrdering.icon.classList.contains("fa-sort-up")
        })
    );
    stateOrdering?.label.addEventListener("click", () =>
        updateRooms({
            orderBy: "state",
            descending: stateOrdering.icon.classList.contains("fa-sort-up")
        })
    );
    participantCountOrdering?.label.addEventListener("click", () =>
        updateRooms({
            orderBy: "participant_count",
            descending: participantCountOrdering.icon.classList.contains("fa-sort-up")
        })
    );

    if (document.readyState === "complete" || document.readyState === "interactive") {
        updateRooms();
    } else {
        window.addEventListener("load", updateRooms);
    }
}
