from __future__ import annotations

import logging
from hashlib import sha1
from typing import TYPE_CHECKING
from urllib.parse import urlencode

import requests
import xmltodict

if TYPE_CHECKING:
    from core.models import Room

logger = logging.getLogger(__name__)


class BigBlueButton:
    def __init__(self, bbb_server_url, bbb_server_salt):
        if not bbb_server_url.startswith("https://"):
            if bbb_server_url.startswith("http://"):
                bbb_server_url = bbb_server_url[:4] + "s" + bbb_server_url[4:]
            else:
                bbb_server_url = "https://{}".format(bbb_server_url)
        if not bbb_server_url.endswith("/bigbluebutton/api/"):
            bbb_server_url = "{}/bigbluebutton/api/".format(bbb_server_url)

        self.__url = bbb_server_url
        self.__salt = bbb_server_salt
        logger.debug(
            "BigBlueButton API Connector object initializes with Values url=%s; salt=%s", self.__url, self.__salt
        )

    def get_api_call_url(self, api_method, api_params):
        checksum = self.create_salt(api_method, api_params)
        if api_params:
            api_params = "{}&".format(api_params)
        api_call_url = "{}{}?{}checksum={}".format(self.__url, api_method, api_params, checksum)

        logger.debug("api_call_url=%s created", api_call_url)
        return api_call_url

    # BBB get meetings
    def get_meetings(self):
        api_call = self.get_api_call_url("getMeetings", "")
        response = self.get_post_request(api_call)

        logger.debug("Execute API call %s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_get_meetings(response):
        meetings = []
        data = xmltodict.parse(response.text)
        logger.debug("Validate response of getMeeting API call (response=%s)", data)

        if data["response"]["returncode"] != "SUCCESS":
            logger.error("Error while make api call getMeetings. data['response']['returncode'] != 'SUCCESS'")
            raise Exception("API Call failed")

        try:
            if data["response"]["meetings"] is not None:
                if type(data["response"]["meetings"]["meeting"]) == list:
                    meetings = data["response"]["meetings"]["meeting"]
                else:
                    meetings.append(data["response"]["meetings"]["meeting"])
        except Exception as e:
            logger.error("Error while validating API call for 'getMeetings' %s", e)
            return None
        logger.debug("Validate response of getMeeting API call completed successfully")

        return meetings

    # BBB create
    def create(self, params, body=None):
        api_call = self.get_api_call_url("create", self.create_params(params))

        # hotfix for creat meeting, since initial create of fresh bbb server takes > 2 sec
        response = self.get_post_request(api_call, body, 5)

        logger.debug("Execute 'create' API call %s; response=%s", api_call, response)

        return response

    @staticmethod
    def validate_create(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'create' API call; result={result}")
            return result
        except KeyError:
            logger.error("create did not respond with a valid XML.")
            return False

    # BBB join
    def join(self, params):
        api_call = self.get_api_call_url("join", self.create_params(params))
        logger.debug("Create 'join' API call=%s", api_call)

        return api_call

    # BBB end
    def end(self, meeting_id, pw):
        api_call = self.get_api_call_url("end", self.create_params({"meetingID": meeting_id, "password": pw}))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'end' API call=%s; response=%s", api_call, response)

        return response

    @staticmethod
    def validate_end(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'end' API call; result={result}")
            return result
        except KeyError:
            logger.error("end did not respond with a valid XML.")
            return False

    # BBB is meeting running
    def is_meeting_running(self, meeting_id):
        api_call = self.get_api_call_url("isMeetingRunning", self.create_params({"meetingID": meeting_id}))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'isMeetingRunning' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_is_meeting_running(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["running"] == "true"
            logger.debug(f"Validating 'isMeetingRunning' API call; result={result}")
            return result
        except KeyError:
            logger.error("isMeetingRunning did not respond with a valid XML.")
            return False

    # BBB get meeting info
    def get_meeting_infos(self, meeting_id):
        api_call = self.get_api_call_url("getMeetingInfo", self.create_params({"meetingID": meeting_id}))
        response = self.get_post_request(api_call)

        logger.debug("Execute 'getMeetingInfo' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_get_meeting_infos(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate getMeetingInfo result=%s", data)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'getMeetingInfo' API call; result={result}")
            return data if result else None
        except KeyError:
            logger.error("getMeetingInfos did not respond with a valid XML.")
            return None

    # BBB create web hook
    def create_web_hook(self, params):
        api_call = self.get_api_call_url("hooks/create", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'hooks/create' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_create_web_hook(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'hooks/create' API call; result={result}")
            return result
        except KeyError:
            logger.error("Webhook hooks/create did not respond with a valid XML.")
            return False

    # BBB get recordings
    def get_recordings(self, params):
        api_call = self.get_api_call_url("getRecordings", self.create_params(params))
        response = self.get_post_request(api_call, timeout=60)
        logger.debug("Execute 'getRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_get_recordings(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'getRecordings' API call; result={result}")
            return data if result else None
        except KeyError:
            logger.error("getRecordings did not respond with a valid XML.")
            return None

    # BBB publish recordings
    def publish_recordings(self, params):
        api_call = self.get_api_call_url("publishRecordings", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'publishRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_publish_recordings(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate publishRecordings result=%s", data)
        try:
            result = data["response"]["published"] == "true"
            logger.debug(f"Validating 'publishRecordings' API call; result={result}")
            return result
        except KeyError:
            logger.error("publishRecordings did not respond with a valid XML.")
            return False

    # BBB delete recording
    def delete_recordings(self, params):
        api_call = self.get_api_call_url("deleteRecordings", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'deleteRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_delete_recordings(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate deleteRecordings result=%s", data)
        try:
            result = data["response"]["deleted"] == "true"
            logger.debug(f"Validating 'deleteRecordings' API call; result={result}")
            return result
        except KeyError:
            logger.error("deleteRecordings did not respond with a valid XML.")
            return False

    # BBB update recording
    def update_recordings(self, params):
        api_call = self.get_api_call_url("updateRecordings", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'updateRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_update_recordings(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate publishRecordings result=%s", data)
        try:
            result = data["response"]["updated"] == "true"
            logger.debug(f"Validating 'publishRecordings' API call; result={result}")
            return result
        except KeyError:
            logger.error("updateRecordings did not respond with a valid XML.")
            return False

    # BBB API utils
    def create_salt(self, api_method, api_params):
        return sha1("{}{}{}".format(api_method, api_params, self.__salt).encode("utf-8")).hexdigest()

    @staticmethod
    def create_params(params):
        return urlencode(params)

    @staticmethod
    def get_post_request(uri, body=None, timeout=2):
        try:
            if body:
                return requests.post(uri, data=body, timeout=timeout)
            return requests.get(uri, timeout=timeout)
        except (ConnectionError, TimeoutError) as e:
            logger.error(f"API Call '{uri}' failed. " + str(e))
            raise e

    @property
    def url(self):
        return self.__url


def validate_bbb_api_call(api_method, params, scheduling_strategy):
    checksum = params.pop("checksum")
    _checksum = sha1(
        "{}{}{}".format(api_method, BigBlueButton.create_params(params), scheduling_strategy.token_registration).encode(
            "utf-8"
        )
    ).hexdigest()
    return True if checksum[0] == _checksum else False


def set_room_config(room: Room) -> None:
    room.mute_on_start = room.config.mute_on_start
    room.moderation_mode = room.config.moderation_mode
    room.everyone_can_start = room.config.everyone_can_start
    room.authenticated_user_can_start = room.config.authenticated_user_can_start
    room.guest_policy = room.config.guest_policy
    room.allow_guest_entry = room.config.allow_guest_entry
    room.access_code = room.config.access_code
    room.only_prompt_guests_for_access_code = room.config.only_prompt_guests_for_access_code
    room.disable_cam = room.config.disable_cam
    room.disable_mic = room.config.disable_mic
    room.allow_recording = room.config.allow_recording
    room.disable_private_chat = room.config.disable_private_chat
    room.disable_public_chat = room.config.disable_public_chat
    room.disable_note = room.config.disable_note
    room.url = room.config.url
    room.dialNumber = room.config.dialNumber
    room.logoutUrl = room.config.logoutUrl
    room.welcome_message = room.config.welcome_message
    room.maxParticipants = room.config.maxParticipants
    room.streamingUrl = room.config.streamingUrl


def check_tenant_permissions(user, tenant) -> bool:
    return user.is_superuser or user.tenant == tenant
