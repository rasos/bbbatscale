import pytest
from core.constants import MODERATION_MODE_MODERATORS
from core.models import Room, SchedulingStrategy, Server, User, get_default_room_config
from core.services import create_join_meeting_url
from django.contrib.sites.models import Site


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        server=example_server,
        name="room",
        meeting_id="12345678",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        moderation_mode=MODERATION_MODE_MODERATORS,
        everyone_can_start=False,
        config=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def example_tenant(db) -> Site:
    return Site.objects.create(name="example.org", domain="example.org")


@pytest.fixture(scope="function")
def user(db, example_tenant) -> User:
    user = User.objects.create(
        username="user",
        email="user@example.org",
        display_name="example_username",
        is_superuser=False,
        is_staff=True,
        tenant=example_tenant,
    )
    return user


def test_create_join_meeting_url(room, user):
    assert create_join_meeting_url(room.meeting_id, "test_username", "test_user_pw") == (
        "https://example.org/bigbluebutton/api/join?meetingID=12345678&password=test_user_pw&"
        "fullName=test_username&redirect=true"
        "&checksum=ac8712dea4035bab196dcf2d5f843a230ba33ca8"
    )

    assert create_join_meeting_url(room.meeting_id, user, "test_user_pw") == (
        "https://example.org/bigbluebutton/api/join?meetingID=12345678&password=test_user_pw&"
        "fullName=example_username&redirect=true&userdata-bbb_auto_join_audio=true&"
        "userdata-bbb_listen_only_mode=true&userdata-bbb_skip_check_audio=false&"
        "userdata-bbb_skip_check_audio_on_first_join=false&userdata-bbb_auto_share_webcam=false&"
        "userdata-bbb_record_video=true&userdata-bbb_skip_video_preview=false&"
        "userdata-bbb_skip_video_preview_on_first_join=false&userdata-bbb_mirror_own_webcam=false&"
        "userdata-bbb_force_restore_presentation_on_new_events=false&userdata-bbb_auto_swap_layout=false&"
        "userdata-bbb_show_participants_on_login=true&userdata-bbb_show_public_chat_on_login=true&"
        "checksum=dcb51003096f6880e93968d90e8459a6bb741c5e"
    )
