import logging
from typing import Dict, Union

from core.models import HomeRoom, PersonalRoom, Room
from core.services import get_rooms_with_current_next_event
from core.views import create_view_access_logging_message
from django.contrib.sites.shortcuts import get_current_site
from django.core.paginator import Paginator
from django.db.models import Q, QuerySet
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from support_chat.models import SupportChatParameter

logger = logging.getLogger(__name__)


def home(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)

    if SupportChatParameter.load(tenant).enabled:
        disable_chat_if_offline = "true" if SupportChatParameter.load(tenant).disable_chat_if_offline else "false"
    else:
        disable_chat_if_offline = "false"
    return render(
        request,
        "home.html",
        {
            "disable_chat_if_offline": disable_chat_if_offline,
        },
    )


def get_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)
    search = request.GET.get("filter")
    rooms_query = get_rooms_with_current_next_event(tenant).filter(
        Q(name__icontains=search)
        | Q(room_occupancy_current__icontains=search)
        | Q(room_occupancy_next__icontains=search)
    )

    order_by = request.GET.get("orderBy")
    if order_by not in ("name", "state", "participant_count"):
        order_by = "name"

    return paginate(
        order_by, request.GET.get("descending", "").lower() == "true", rooms_query, int(request.GET.get("page", 1))
    )


def get_personal_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)
    search = request.GET.get("filter")
    rooms_query = PersonalRoom.objects.filter(
        Q(name__icontains=search) | Q(owner__first_name__icontains=search) | Q(owner__last_name__icontains=search),
        is_breakout=False,
        config__isnull=False,
        tenants__in=[tenant],
        is_public=True,
    ).prefetch_related("config", "owner")

    order_by = request.GET.get("orderBy")
    if order_by not in ("name", "owner", "state", "participant_count"):
        order_by = "name"

    if order_by == "owner":
        order_by = "owner__last_name"

    return paginate(
        order_by, request.GET.get("descending", "").lower() == "true", rooms_query, int(request.GET.get("page", 1))
    )


def get_home_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)
    search = request.GET.get("filter")
    rooms_query = HomeRoom.objects.filter(
        Q(owner__first_name__icontains=search) | Q(owner__last_name__icontains=search),
        is_breakout=False,
        config__isnull=False,
        tenants__in=[tenant],
        is_public=True,
    ).prefetch_related("config", "owner")

    order_by = request.GET.get("orderBy")
    if order_by not in ("owner", "state", "participant_count"):
        order_by = "owner"

    if order_by == "owner":
        order_by = "owner__last_name"

    return paginate(
        order_by, request.GET.get("descending", "").lower() == "true", rooms_query, int(request.GET.get("page", 1))
    )


def paginate(order_by: str, descending: bool, query: QuerySet, page: int) -> JsonResponse:
    prefix = "-" if descending else ""

    # Always order by pk as fallback to prevent inconsistency with non-unique values
    paginator = Paginator(query.order_by(prefix + order_by, prefix + "pk"), 50)

    try:
        page_number = max(1, min(paginator.num_pages, int(page)))
    except ValueError:
        page_number = 1

    def map_room(room: Room) -> Dict[str, Union[str, int, None]]:
        mapped_room = {
            "url": reverse("join_redirect", args=[room.name]),
            "name": room.name,
            "state": room.state,
            "configuration": room.config.name,
            "participants": room.participant_count,
            "comment": room.comment_public,
        }

        if isinstance(room, (HomeRoom, PersonalRoom)):
            mapped_room["owner"] = str(room.owner)

        if hasattr(room, "room_occupancy_current") and hasattr(room, "room_occupancy_next"):
            mapped_room["current_occupancy"] = room.room_occupancy_current
            mapped_room["next_occupancy"] = room.room_occupancy_next

        return mapped_room

    return JsonResponse(
        {
            "page": page_number,
            "page_count": paginator.num_pages,
            "rooms": [map_room(room) for room in paginator.page(page_number).object_list],
        }
    )
