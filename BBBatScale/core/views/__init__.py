from django.http import HttpRequest


def create_view_access_logging_message(request: HttpRequest, *args):
    msg = "{}-view accessed by 'username'= '{}'; ".format(request.resolver_match.view_name, request.user.username)

    msg += "Parameters="
    for arg in args:
        msg += "{}; ".format(arg)

    if "HTTP_USER_AGENT" in request.META:
        msg += "'HTTP_USER_AGENT'='{}'; ".format(request.META["HTTP_USER_AGENT"])
    else:
        msg += "'HTTP_USER_AGENT'='{}'; ".format("None")

    if "HTTP_HOST" in request.META:
        msg += "'HTTP_HOST'='{})".format(request.META["HTTP_HOST"])
    else:
        msg += "'HTTP_HOST'='{})".format("None")

    return msg
