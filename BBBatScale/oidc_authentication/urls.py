from django.urls import path
from mozilla_django_oidc.views import OIDCAuthenticationCallbackView
from oidc_authentication import views

urlpatterns = [
    path("callback/", OIDCAuthenticationCallbackView.as_view(), name="oidc_authentication_callback"),
    path("authenticate/", views.TenantOIDCAuthenticationRequestView.as_view(), name="oidc_authentication_init"),
    path("logout/", views.TenantOIDCLogoutView.as_view(), name="oidc_logout"),
]
