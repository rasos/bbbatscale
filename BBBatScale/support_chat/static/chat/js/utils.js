/**
 * Updates the supplied badge.
 * If the count is zero, the badge will be hidden, otherwise the text inside and the tooltip text will be updated.
 *
 * @param {jQuery} badge The badge to update.
 * @param {number} count The new count which will be used to update the badge.
 */
function updateUnreadMessagesBadge(badge, count) {
    if (count === 0) {
        badge.text(null);
        badge.attr('title', null);
    } else {
        badge.text(count.toString());
        badge.attr('title', interpolate(ngettext('%s Message', '%s Messages', count), [count]));
    }
}
