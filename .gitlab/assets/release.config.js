"use strict";

const { releaseRules, sectionMap } = require("./commit-release-rules");
const { open, readFile, writeFile } = require("fs/promises");
const { resolve } = require("path");

const commitAssetPaths = {
    chartPath: "charts/bbbatscale/Chart.yaml",
    changelogPath: "CHANGELOG.rst"
};

const ownerFormat = "{{#if this.owner}}{{~this.owner}}{{else}}{{~@root.owner}}{{/if}}";
const hostFormat = "{{~@root.host}}";
const repositoryFormat = "{{#if this.repository}}{{~this.repository}}{{else}}{{~@root.repository}}{{/if}}";

const releaseNotesGeneratorConfig = {
    preset: "conventionalcommits",
    parserOpts: {
        noteKeywords: ["BREAKING CHANGE", "BREAKING-CHANGE"]
    },
    writerOpts: {
        commitsSort(lhs, rhs) {
            let result = (lhs.scope || "").localeCompare(rhs.scope || "");
            if (!lhs.scope || !rhs.scope) {
                result = -result;
            }

            if (result === 0) {
                result = (lhs.subject || "").localeCompare(rhs.subject || "");
                if (!lhs.subject || !rhs.subject) {
                    result = -result;
                }
            }
            return result;
        },
        notesSort(lhs, rhs) {
            let result = (lhs.commit.scope || "").localeCompare(rhs.commit.scope || "");
            if (!lhs.commit.scope || !rhs.commit.scope) {
                result = -result;
            }

            if (result === 0) {
                result = (lhs.text || "").localeCompare(rhs.text || "");
                if (!lhs.text || !rhs.text) {
                    result = -result;
                }
            }
            return result;
        },
        issueUrlFormat: `${hostFormat}/${ownerFormat}/${repositoryFormat}/issues/{{this.issue}}`,
        commitUrlFormat: `${hostFormat}/${ownerFormat}/${repositoryFormat}/commit/{{hash}}`,
        compareUrlFormat: `${hostFormat}/${ownerFormat}/${repositoryFormat}/compare/{{previousTag}}...{{currentTag}}`,
        userUrlFormat: `${hostFormat}/{{user}}`,
        issuePrefixes: ["#"]
    },
    presetConfig: {
        types: sectionMap
    }
};

module.exports = {
    plugins: [
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "conventionalcommits",
                releaseRules: releaseRules,
                parserOpts: {
                    noteKeywords: ["BREAKING CHANGE", "BREAKING-CHANGE"]
                }
            }
        ],
        ["@semantic-release/release-notes-generator", releaseNotesGeneratorConfig],
        [
            "@semantic-release/gitlab",
            {
                gitlabUrl: "https://gitlab.com/"
            }
        ],
        {
            async prepare(_, { nextRelease }) {
                const yaml = require("js-yaml");

                const chart = yaml.load(await readFile(commitAssetPaths.chartPath, "utf8"), {
                    schema: yaml.FAILSAFE_SCHEMA
                });
                chart.appVersion = nextRelease.version;
                await writeFile(commitAssetPaths.chartPath, yaml.dump(chart));
            },
            publish(_, { nextRelease }) {
                craneLogin(process.env.CI_REGISTRY_USER, process.env.CI_REGISTRY_PASSWORD, process.env.CI_REGISTRY);
                copyImages(nextRelease.version, process.env.BBBATSCALE_IMAGE, process.env.NGINX_IMAGE);

                const remoteRegistry = process.env.REMOTE_REGISTRY;
                const remoteRegistryUsername = process.env.REMOTE_REGISTRY_USERNAME;
                const remoteRegistryPassword = process.env.REMOTE_REGISTRY_PASSWORD;
                if (remoteRegistry && remoteRegistryUsername && remoteRegistryPassword) {
                    craneLogin(remoteRegistryUsername, remoteRegistryPassword, remoteRegistry);
                    copyImages(
                        nextRelease.version,
                        process.env.REMOTE_BBBATSCALE_IMAGE,
                        process.env.REMOTE_NGINX_IMAGE
                    );
                }
            }
        },
        {
            async prepare(pluginConfig, context) {
                const changelogHandle = await open(commitAssetPaths.changelogPath, "a");

                try {
                    if ((await changelogHandle.stat()).size === 0) {
                        await changelogHandle.write(
                            "===========\n Changelog\n===========\n\n.. contents:: Versions\n  :depth: 1\n",
                            undefined,
                            "utf8"
                        );
                    }

                    await changelogHandle.write(
                        "\n" + (await rstChangelog(pluginConfig, context)) + "\n",
                        undefined,
                        "utf8"
                    );
                } finally {
                    await changelogHandle.close();
                }
            }
        },
        [
            "@semantic-release/git",
            {
                assets: Object.values(commitAssetPaths),
                message:
                    "release(<%= nextRelease.version %>): auto generated release version <%= nextRelease.version %> at <%= new Date().toISOString() %>\n\n<%= nextRelease.notes %>"
            }
        ]
    ],
    tagFormat: "${version}"
};

async function rstChangelog(pluginConfig, context) {
    const { generateNotes } = require("@semantic-release/release-notes-generator");
    const Handlebars = require("handlebars");

    const config = {
        ...pluginConfig,
        ...releaseNotesGeneratorConfig,
        writerOpts: {
            ...pluginConfig.writerOpts,
            ...releaseNotesGeneratorConfig.writerOpts,
            mainTemplate: await readFile(resolve(__dirname, "rst-templates/template.hbs"), "utf8"),
            headerPartial: (await readFile(resolve(__dirname, "rst-templates/header.hbs"), "utf8")).replace(
                /{{compareUrlFormat}}/g,
                releaseNotesGeneratorConfig.writerOpts.compareUrlFormat
            ),
            commitPartial: (await readFile(resolve(__dirname, "rst-templates/commit.hbs"), "utf8"))
                .replace(/{{commitUrlFormat}}/g, releaseNotesGeneratorConfig.writerOpts.commitUrlFormat)
                .replace(/{{issueUrlFormat}}/g, releaseNotesGeneratorConfig.writerOpts.issueUrlFormat),
            footerPartial: ""
        }
    };

    Handlebars.registerHelper({
        formatNote: function (text) {
            // If the note contains a list keep it, otherwise join the lines.
            const lines = text.split("\n");
            if (
                lines.every((line) =>
                    ["* ", "+ ", "- ", "• ", "‣ ", "⁃ ", "  "].some((prefix) => line.startsWith(prefix))
                )
            ) {
                return "\n\n" + lines.map((line) => "  " + line).join("\n");
            }
            return lines.join(" ");
        },
        underline: function (char, options) {
            const text = options.fn(this);
            return text + "\n" + char.repeat(text.length);
        },
        reformatMarkdown: function (options) {
            let text = options.fn(this);

            // inline code
            text = text.replace(
                /(`[^`]*?`)(.?)/g,
                (_, inlineCode, suffix) => inlineCode + ":code:" + (/^\s?$/.test(suffix) ? "" : "\\") + suffix
            );
            // urls
            text = text.replace(/\[([^\]]*?)]\((.*?)\)/g, "`$1 <$2>`_");

            return text;
        }
    });

    try {
        return (await generateNotes(config, context)).trim();
    } finally {
        Handlebars.unregisterHelper("formatNote");
        Handlebars.unregisterHelper("underline");
    }
}

function crane(...args) {
    console.log(require("child_process").execFileSync("/usr/local/bin/crane", args, { encoding: "utf8" }));
}

function craneLogin(username, password, registry) {
    crane("auth", "login", "--username", username, "--password", password, registry);
}

function craneCopy(from, to) {
    crane("copy", from, to);
}

function copyImages(version, bbbAtScaleImage, nginxImage) {
    if (bbbAtScaleImage) {
        craneCopy(`${process.env.BBBATSCALE_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${bbbAtScaleImage}:latest`);
        craneCopy(`${process.env.BBBATSCALE_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${bbbAtScaleImage}:${version}`);
    }

    if (nginxImage) {
        craneCopy(`${process.env.NGINX_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${nginxImage}:latest`);
        craneCopy(`${process.env.NGINX_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${nginxImage}:${version}`);
    }
}
