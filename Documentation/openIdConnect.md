# Documentation for the BBBatScale OpenID Connect implementation

The OpenID Connect implementation has only been tested with KeyCloak as OpenID Provider.  
While it should generally work with other options, we recommend to use KeyCloak.


## Usage
As superuser, you have option to create tenant based authentication parameter.
This can be configured for each tenant individually.
If you don't choose to configure the authentication parameter, the standard django model backend will be used for this tenant.

### OIDC Endpoint Configuration
If you are using keycloak, please have a look at [official documentation](https://www.keycloak.org/docs/4.8/authorization_services/) to find your necessary endpoints.

# Claims

## Standard Claims

* **`given_name`**: the first name of the user, defaults to an empty string
* **`family_name`**: the last name of the user, defaults to an empty string
* **`name`**: the display name of the user, defaults to an empty string
* **`email`**: the email of the user, defaults to an empty string

## Custom Claims
Custom claims are used to give more permissions to a user.
For following roles the attribute inside a `groups` claim shall be provided.

* **Student/Normal user**: No custom claims required
* **Teacher/Moderator**: An `moderator` entry inside the `groups` claim expected
* **Admin/Superuser**: An `superuser` entry inside the `groups` claim expected
* **Staff**: An `staff` entry inside the `groups` claim expected
* **Supporter**: An `supporter` entry inside the `groups` claim expected

## preferred_username
* **`preferred_username`**: If you have already user inside django, you can provide this claim which directs to the username of django. 
  If provided, BBB@Scale will merge the existing user, his personal and private rooms as well as all meetings related to this user.
